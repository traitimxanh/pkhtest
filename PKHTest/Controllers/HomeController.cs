﻿using PKHTest.Models;
using SocialNetwork.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PKHTest.Controllers
{
    public class HomeController : BaseController
    {
        PKHAdminEntities db = new PKHAdminEntities();

        static string Layout = "_Layout";

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View("Index", Layout, db.News);
        }

        [HttpPost]
        public ActionResult Index(string layout)
        {
            Layout = layout;
            return View("Index", Layout, db.News);
        }

        //[AllowAnonymous]
        //[ActionName("tao-post")]
        public ActionResult CreateNews()
        {
            ViewBag.Catetories = db.Categories;
            return View("CreateNews", Layout);
        }


        [AllowAnonymous]
        public ActionResult Detail(int id)
        {
            return View("Detail", Layout, db.vNews.Find(id));
        }
    }
}
