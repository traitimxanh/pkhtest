﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PKHTest.Models;

namespace PKHTest.Controllers
{
    public class NewsController : ApiController
    {
        private PKHAdminEntities db = new PKHAdminEntities();

        [Route("api/news/create")]
        public IHttpActionResult CreateNews(News model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            model.CreatedDate = DateTime.Now;
            db.News.Add(model);
            db.SaveChanges();
            return Ok(model.ID);
        }

        [Route("api/user/send_message")]
        public IHttpActionResult SendMessage(Comment model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            model.CreatedBy = "admin";
            db.Comments.Add(model);
            db.SaveChanges();
            return Ok();
        }

        [Route("api/user/list_message")]
        [HttpGet]
        public IHttpActionResult ListMessage(int? newsID)
        {
            var list = db.Comments.Where(o => o.NewsID == newsID).ToList();
            return Ok(list);
        }
    }
}