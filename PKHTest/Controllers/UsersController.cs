﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PKHTest.Models;

namespace PKHTest.Controllers
{
    public class UsersController : Controller
    {
        private PKHTestEntities db = new PKHTestEntities();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User model)
        {
            var user = db.Users.SingleOrDefault(o => o.Username == model.Username && o.Password == model.Password);
            if (user != null) {
                Session.Add("USER_SESSION", model.Username);
                return RedirectToAction("Index", "Home");
            }
            
            return View(model);
        }
        
    }
}
