﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace PKHTest
{
    public static class StringHelpers
    {
        public static string ToSeoUrl(this string url)
        {
            for (int i = 33; i < 48; i++)
            {
                url = url.Replace(((char)i).ToString(), "");
            }

            for (int i = 58; i < 65; i++)
            {
                url = url.Replace(((char)i).ToString(), "");
            }

            for (int i = 91; i < 97; i++)
            {
                url = url.Replace(((char)i).ToString(), "");
            }
            for (int i = 123; i < 127; i++)
            {
                url = url.Replace(((char)i).ToString(), "");
            }
            url = url.Replace(" ", "-");
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = url.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');

            //// make the url lowercase
            //string encodedUrl = (url ?? "").ToLower();

            //// replace & with and
            //encodedUrl = Regex.Replace(encodedUrl, @"\&+", "and");

            //// remove characters
            //encodedUrl = encodedUrl.Replace("'", "");

            //// remove invalid characters
            //encodedUrl = Regex.Replace(encodedUrl, @"[^a-z0-9]", "-");

            //// remove duplicates
            //encodedUrl = Regex.Replace(encodedUrl, @"-+", "-");

            //// trim leading & trailing characters
            //encodedUrl = encodedUrl.Trim('-');

            //return encodedUrl;
        }
    }
}